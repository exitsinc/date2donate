	<?php include('includes/main_header.php'); ?>

	<section class="home-about-area bike pt-120">
		<div class="container">
			<div class="">
				<div class="" style="width: 100%;">
					<div class="cust-cursor" onclick="thevid=document.getElementById('thevideo'); thevid.style.display='block'; this.style.display='none'">
						<img style=" width:100%;" alt="" src="img/ban.png" />
					</div>
					<div class="youtube-video" id="thevideo" style="display: none;">
						<iframe width="100%" height="500" src="https://www.youtube.com/embed/VqR1lAB0MAk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="home-about-area pt-120 OP">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="">
						<h1 class="text-white about-text mb-5">Date2Donate enables you to find love by spreading love.</h1>
						<p class="text-white" style="font-size: calc(.6vw + 1rem);">Date2Donate is a platform for singletons to meet and make connections through our online platform, singles events and ambassador parties. </p>
						<p class="text-white" style="font-size: calc(.6vw + 1rem);">We give our members an opportunity to build long lasting friendships and relationship all while doing something good at the same time: as all of our profits are used to help amazing community projects and charity partners. </p>
						<a href="#" class="btn btn-black btn-lg mt-5" style="padding: 10px 30px;font-size: 1.3em;font-weight: 600;">Find Out More</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End home-about Area -->

	<section class="services-area  card-section getdating">
		<div class="container">
			<div class="row d-flex justify-content-left">
				<div class="menu-content  col-lg-12 col-md-12 col-sm-3">
					<div class="title text-left  ">
						<h1 class="about-text mb-5">Get Dating</h1>
						<p class="text-white" style="font-size: calc(.6vw + 1rem);">Explore the ways you can feel the butterflies and find your match at our events.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="services-area section-gap card-section">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-6 mt-3">
					<div class="card" style="width: 100%;background: unset;border: unset;">
						<img class="card-img-top" style="max-height:250px" src="img/date1.jpg" alt="Card image cap">
						<div class="dating-card-body">
							<h3 class="card-title text-black mt-3">Online Speed Dating</h3>
							<p class="text-black">It’s just like speed dating in-person except is on your Phone, PC, Laptop or Tablet.</p>
							<a href="#" class="btn btn-black btn-lg mt-5">Find Out More</a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mt-3">
					<div class="card" style="width: 100%;background: unset;border: unset;">
						<img class="card-img-top" style="max-height:250px" src="img/date3.jpg" alt="Card image cap">
						<div class="dating-card-body">
							<h3 class="card-title text-black mt-3">Ambassador Singles Events</h3>
							<p class="text-black">Our ambassadors love our idea and are keen to help us get the word out about our vision</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 mt-3">
					<div class="card" style="width: 100%;background: unset;border: unset;">
						<img class="card-img-top" style="max-height:250px" src="img/date2.jpg" alt="Card image cap">
						<div class="dating-card-body">
							<h3 class="card-title text-black mt-3">Our Singles Events</h3>
							<p class="text-black">Once we are fully up and running we will host our own singles mixer events.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<section class="services-area  section-gap lsound">
		<div class="container">
			<div class="row d-flex justify-content-left">
				<div class="menu-content  col-lg-12 col-md-8 col-sm-12">
					<div class=" ">
						<h1 class="about-text">Love the sound of Date2Donate, become…</h1>

					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="recent-blog-area section-gap">
		<div class="container">

			<div class="row">
				<div class="col-lg-6 col-md-6 py-2">
					<div class="card p-5 h-100" style="width: 100%;background: unset;border: unset;">
						<img class="card-img-top" style="max-height:250px" src="img/lts.png" alt="Card image cap">
						<div class="dating-card-body d-flex flex-column h-100">
							<h1 class="card-title text-black mt-3">Our Ambassador host</h1>
							<p class="text-black">As a host Ambassador you’ll share the same moral ethos as us, you’ll want to be spreader of love and in the process raise funds for charities by running your own Date2Donate events.</p>
							<a href="#" class="btn btn-black btn-lg align-self-start" style="margin-top: auto!important;">Find Out More</a>
						</div>
					</div>
				</div>
				<div class="col-lg-6 col-md-6 py-2">
					<div class="card p-5 h-100" style="width: 100%;background: unset;border: unset;">
						<img class="card-img-top" style="max-height:250px" src="img/lts2.jpg" alt="Card image cap">
						<div class="dating-card-body d-flex flex-column h-100">
							<h1 class="card-title text-black mt-3">Our Partner in crime</h1>
							<p class="text-black">We’re looking for partners and sponsors that are head over heals for us and are able to offer Date2Donate support in faciliating our events and help to grow our community.</p>
							<a href="#" class="btn btn-black btn-lg align-self-start" style="margin-top: auto!important;">Find Out More</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php include('includes/main_footer.php'); ?>