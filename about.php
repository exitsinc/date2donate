	<?php include('includes/main_header.php'); ?>
	<section class="">
		<div class="custom-banner">
			<img class="banner-img" src="img/ban3.png" alt="">
		</div>
	</section>

	<section class="home-about-area pt-120 OP">
		<div class="container">
			<div class="row align-items-center justify-content-between">
				<div class="col-lg-12 col-md-12 col-sm-3 home-about-left">
					<div class="homei">
						<h1 class="about-text mb-3">We are saying NO - to profiting from love!</h1>

						<p class="text-white" style="font-size: calc(.6vw + 1rem);">Dating sites are taking world by storm; with almost a third of single people having used or currently using an online dating platform or app. Some of which require a subscription fee while others demand money to access additional features. At Date2Donate, we enable our members to find love while spreading love!?! Whatever way we get you dating, our community is making a difference. </p>
						<p class="text-white" style="font-size: calc(.6vw + 1rem);">Check out our Get Dating page to start your dating journey with us! </p>
						<a href="services.php"><button type="button" class="btn btn-black btn-lg">Get Dating</button></a>
					</div>
				</div>
			</div>
		</div>
	</section>
					
			</section> -->
	<!-- End home-about Area -->
	<section class="services-area  card-section getdating">
		<div class="container">
			<div class="row d-flex justify-content-left">
				<div class="menu-content  col-lg-12 col-md-12 col-sm-3">
					<div class="title ">
						<h1 class="mb-10 text-right about-text">Our Charity Partners</h1>
						<p class="text-white text-center" style="font-size: calc(3px + 1rem);">At Date2Donate you can find comfort in knowing every date is making a difference and supporting charties..

							Each year we support 1 - 3 charities. Date2Donate tends to support smaller charites where we can develop and maintain close relationships, this enables us to see first hand the difference our community’s contribution makes. Moreover these meanigful relationships resonate with our members and resembles the connections you’ll make on our platform. This year our fundraising efforts will be aiding; Doors of Hope Zambia, Singing Hands and Step by Step.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="services-area section-gap card-section">
		<div class="container">

			<div class="row">
				<div class="col-lg-4 col-md-6">
					<div class="single-services">
						<div class="part">
							<a href="#"><img src="img/part1.png"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services">
						<div class="part">
							<a href="#"><img src="img/part2.png"></a>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6">
					<div class="single-services">
						<div class="part">
							<a href="#"><img src="img/part3.png"></a>
						</div>
					</div>
				</div>
			</div>

		</div>
	</section>


	<section class="recent-blog-area section-gap ">
		<div class="s-container">
			<div class="row">
				<div class=" col-lg-6 col-md-6 mt-5">
					<h2 class="about-text text-center">Press Office</h2>
					<p class="text-left" style="font-size: calc(3px + 1rem);font-weight:bold;color:#000">
						Whatever the way you tell our story, you can make a difference. Free leaflets, posters and articles are available when you download our press kit.
					</p>
					<a href="services.php"><button type="button" class="btn btn-black btn-lg">Download our kit</button></a>
				</div>
				<div class=" col-lg-6 col-md-6 mt-5">
					<h2 class="about-text text-center">Donate</h2>
					<p class="text-left" style="font-size: calc(3px + 1rem);font-weight:bold;color:#000">
						If you can’t make it to one of our events but our mission resonates with you, you can still pay us a compliment and show compation by making a donation.
					</p>
					<a href="services.php"><button type="button" class="btn btn-black btn-lg">Make a Donation</button></a>
				</div>
			</div>
		</div>
	</section>


	<?php include('includes/main_footer.php'); ?>