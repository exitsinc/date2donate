<?php include('includes/main_header.php'); ?>
<section>
	<div class="dating-banner">
		<h3 class="text-white" style="font-size:calc(8vw);margin-top: calc(14vw);">Get Dating</h3>
	</div>
</section>
<!-- End banner Area -->


<!-- Start services Area -->
<section class="services-area section-gap pt-120">
	<div class="s-container">
		<div class="row d-flex justify-content-center">
			<div class="menu-content  col-lg-7">
				<div class="title">
					<h1 class="mb-5 about-text">Join us at one of our events!</h1>

				</div>
			</div>
		</div>
		<div class="row justify-content-right">
			<div class="col-lg-4 col-md-6 text-center">
				<div class="card p-5 h-100" style="width: 100%;background: unset;border: unset;">
					<img class="card-img-top" style="max-height:250px" src="img/Osd1.png" alt="Card image cap">
					<div class="dating-card-body d-flex flex-column h-100 text-center">
						<h2 class="card-title text-white mt-3">Online Speed Dating</h2>
						<p class="text-white">It’s just like speed dating in-person except is on your Phone, PC, Laptop or Tablet.</p>
						<p class="text-white">You’ll meet a number of other singles - without having swipe right for ages and from the comfort of your home.</p>
						<a href="#" class="btn btn-black btn-lg align-self-start" style="margin-top: auto!important;font-weight: bold;">Date & Venue</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6 text-center">
				<div class="card p-5 h-100" style="width: 100%;background: unset;border: unset;">
					<img class="card-img-top" style="max-height:250px" src="img/Osd2.png" alt="Card image cap">
					<div class="dating-card-body d-flex flex-column h-100 text-center">
						<h2 class="card-title text-white mt-3">Wine and Dine</h2>
						<p class="text-white">During this evening not only will you get to sample some fine wines and other treats.</p>
						<p class="text-white">You will meet other singletons, give your taste buds a treat as well as discover your perfect dinning and winning match.</p>
						<a href="#" class="btn btn-black btn-lg align-self-start" style="margin-top: auto!important;font-weight: bold;">Event List</a>
					</div>
				</div>
				
			</div>
			<div class="col-lg-4 col-md-6 text-center">
				<div class="card p-5 h-100" style="width: 100%;background: unset;border: unset;">
					<img class="card-img-top" style="max-height:250px" src="img/Osd.png" alt="Card image cap">
					<div class="dating-card-body d-flex flex-column h-100 text-center">
						<h2 class="card-title text-white mt-3">Quiz and games</h2>
						<p class="text-white">Fed up of never meeting anyone in person? Speed dating is not your cup ot tea.</p>
						<p class="text-white">Come to our games or quiz nights. You going to have to chat with your team mates and you might even win a date.</p>
						<a href="#" class="btn btn-black btn-lg align-self-start" style="margin-top: auto!important;font-weight: bold;">Event Details</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Start price Area -->
<section class="price-area section-gap pink">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="menu-content pb-70 col-lg-8">
				<div class="title text-center">
					<h1 class="mb-10 about-text">Our Events Calendar</h1>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-6 single-price">
				<div class="top-part">
					<h1 class="package-no">01</h1>
					<h4>Economy</h4>
					<p class="mt-10">For the individuals</p>
				</div>
				<div class="package-list">
					<ul>
						<li>Secure Online Transfer</li>
						<li>Unlimited Styles for interface</li>
						<li>Reliable Customer Service</li>
					</ul>
				</div>
				<div class="bottom-part">
					<h1>£199.00</h1>
					<a class="price-btn text-uppercase" href="#">Buy Now</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 single-price">
				<div class="top-part">
					<h1 class="package-no">02</h1>
					<h4>Business</h4>
					<p class="mt-10">For the individuals</p>
				</div>
				<div class="package-list">
					<ul>
						<li>Secure Online Transfer</li>
						<li>Unlimited Styles for interface</li>
						<li>Reliable Customer Service</li>
					</ul>
				</div>
				<div class="bottom-part">
					<h1>£299.00</h1>
					<a class="price-btn text-uppercase" href="#">Buy Now</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 single-price">
				<div class="top-part">
					<h1 class="package-no">03</h1>
					<h4>Premium</h4>
					<p class="mt-10">For the individuals</p>
				</div>
				<div class="package-list">
					<ul>
						<li>Secure Online Transfer</li>
						<li>Unlimited Styles for interface</li>
						<li>Reliable Customer Service</li>
					</ul>
				</div>
				<div class="bottom-part">
					<h1>£399.00</h1>
					<a class="price-btn text-uppercase" href="#">Buy Now</a>
				</div>
			</div>
			<div class="col-lg-3 col-md-6 single-price">
				<div class="top-part">
					<h1 class="package-no">04</h1>
					<h4>Exclusive</h4>
					<p class="mt-10">For the individuals</p>
				</div>
				<div class="package-list">
					<ul>
						<li>Secure Online Transfer</li>
						<li>Unlimited Styles for interface</li>
						<li>Reliable Customer Service</li>
					</ul>
				</div>
				<div class="bottom-part">
					<h1>£499.00</h1>
					<a class="price-btn text-uppercase" href="#">Buy Now</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End price Area -->

<!-- Start testimonial Area -->
<section class="testimonial-area section-gap green">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="menu-content  col-lg-8">
				<div class="title text-center">
					<h1 class="mb-10 text-white">GO on feel good about yourself and click RSVP to see for what we do for yourself</h1>
					<a href="#"><button type="button" class="btn btn-black btn-lg">RSVP HERE</button></a>
				</div>
			</div>
		</div>

	</div>
</section>
<!-- End testimonial Area -->

<!-- start footer Area -->
<?php include('includes/main_footer.php'); ?>