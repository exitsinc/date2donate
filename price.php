	<?php include('includes/main_header.php'); ?>


	<!-- start banner Area -->
	<section class="Ambassador">
		<div class="s-container">
			<div class="row d-flex align-items-center justify-content-center">
				<div class="about-content col-lg-12 col-md-12 col-sm-6">
					<h1 class="about-text">
						Be an Ambassador of Love
					</h1>
					<p class="text-white" style="font-size: calc(.24vw + 1rem);">
						Want to help others to find love while spreading love? Become a Robin Hood of love and spread our mission in your city,
						university or amongst your friends and host your own singles party to raise funds for us.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- End banner Area -->



	<section class="Ambassador">
		<div class="s-container">
			<div class="row hosting" style="width: 100%;">
				<div class="col-12 col-left">
					<div class="col-left-img">
						<img class="" src="img/Women.png" alt="" style="background-color: #1cb7d0;width:100%;height:100%">
					</div>
					<div class="col-left-card">
						<div class="card " style="width:45%">
							<div class="card-body host">
								<h3 class="card-title">Be Dining Host </h3>

								<p class="card-text">If you believe that food is a way to majority of hearts. </p>
								<p class="card-text">Organise ‘come dine with me’ style evening, where your single friends would bring a particular dish they would cook for their perfect dinner date. </p>
								<p class="card-text">Not only will the attendees fill their bellies during the evening, but they would be able to discover their perfect dinning match. </p>
								<a href="#" class="btn btn-lg btn-black">Start Hosting</a>

							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-right">
					<div class="col-right-img">
						<img class="" src="img/dho6.png" alt="" style="background-color: #1cb7d0;width:100%;height:100%">
					</div>
					<div class="col-right-card">
						<div class="card " style="width:45%">
							<div class="card-body host">
								<h3 class="card-title">Be Pairing Connoisseur</h3>

								<p class="card-text">Organise your own wine tasting event, for all your singles who are ready to mingle and appreciate a good glass of wine. </p>
								<p class="card-text">And you’ll see - not only would a bunch of single souls have had a great time while raising fund for us but by the end of this evening they sure would have discovered their winning soul mate!!! </p>
								<a href="#" class="btn btn-lg btn-black">Start Pairing</a>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	

	<section class="timeline pb-120 games-night">
		<div class="s-container">
			<div class="text-center">
				<div class="menu-content pb-70">
					<div class="title text-center">
						<h1 class="mb-10">Games nights</h1>
						<p>No no is not love games that we are referring as the options for this type of singles gathering really are endless and can be adapted depending on the types of resources available and whether people want to play in teams or individually.</p>

						<p>Try the old classics such as Bingo, Bridge, Monopoly or traditional Quiz night through to ideas inspired by game shows such as ‘Minute to Win It’, ‘8 out of 10 Cats’</p>
						<p> Often these inclusive atmospheres can relieve some of the pressure that is otherwise felt on a one-to-one first date.</p>
					</div>
				</div>
				<div class="game">
					<img class="img-fluid" src="img/dho5.png" alt="">

				</div>
				<a href="#" class="btn btn-lg btn-black">Start Orgnaising Your Game</a>
			</div>
		</div>
	</section>


	<!-- new layout -->
	<section class="services-area section-gap green ">
		<div class="s-container">
			<div class="row d-flex justify-content-left become">
				<div class="menu-content  col-lg-12">
					<div class="title text-right">
						<h1 class="mb-10 about-text	">Become our Partner</h1>
						<p style="font-size: calc(.6vw + 1rem);">Everyone loves a business that gives back, join us as Date2Donate affiliate, help us to facilitate our events and to grow our community.</p>
					</div>
				</div>
			</div>

			<div class="row hosting" style="width: 100%;">
				<div class="col-12 col-right">
					<div class="col-right-img" style="width:60%">
						<img class="" src="img/dho4.png" alt="" style="background-color:#6AB325;width:100%;height:100%">
					</div>
					<div class="col-right-card">
						<div class="card " style="width:45%">
							<div class="card-body host">
								<h3 class="card-title">Have a venue?</h3>

								<p class="card-text">You think your customers would love the sound of Date2Donate, if so, please do invite us to host one of our events at your premisses. </p>
								<a href="contact.php#contactus" class="btn btn-lg btn-black">Get in Touch</a>
							</div>
						</div>
					</div>
				</div>
				<div class="col-12 col-left">
					<div class="col-left-img" style="width:60%">
						<img class="" src="img/dho2.png" alt="" style="background-color:#6AB325;width:100%;height:100%">
					</div>
					<div class="col-left-card">
						<div class="card " style="width:45%">
							<div class="card-body host">
								<h3 class="card-title">Have a product? </h3>

								<p class="card-text">If your organisation would like to donate something that you think Date2Donate community would love, please do get in touch.  </p>
								<a href="contact.php#contactus" class="btn btn-lg btn-black">Get in Touch</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- end -->


	<section class="timeline pb-120 Our-Part green">
		<div class="s-container">
			<div class="text-center">
				<div class="menu-content pb-70">
					<div class="title text-center">
						<h1 class="mb-10 about-text ">Vochers welcomed </h1>
						<p class="text-white" style="font-size: calc(.6vw + 1rem);">Think your business is and ideal nest for a first date or for romatic get way? Please do send us a vocher and we will make sure to gift it to our members who are struggling. </p>
					</div>
				</div>
				<div class="game">
					<img class="img-fluid" src="img/dho3.png" alt="">

				</div>
				<a href="#" class="btn btn-lg btn-black">Donate Vocher Here</a>
			</div>
		</div>
	</section>




	<!-- End faq Area -->

	<!-- start footer Area -->
	<?php include('includes/main_footer.php'); ?>