<?php include('includes/main_header.php'); ?>

<!-- start banner Area -->
<section class="">
	<div class="custom-banner">
		<img class="banner-img" src="img/blog.png" alt="">
	</div>
</section>
<!-- End banner Area -->
<section class="services-area section-gap pt-120">
	<div class="container">
		<div class="row d-flex justify-content-center">
			<div class="menu-content  col-lg-7">
				<div class="title text-center">
					<h1 class="mb-10 about-text">Make it stand out.</h1>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-6">
				<div class="card" style="width: 100%;background: unset;border: unset;">
					<!-- <img class="card-img-top" style="max-height:250px" src="img/Osd1.png" alt="Card image cap"> -->
					<div class="dating-card-body text-center">
						<h1 class="card-title text-white mt-3">Story behind Doors of Hope Zambia</h1>
						<p class=" text-center">To click into the articles text.</p>

					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="card" style="width: 100%;background: unset;border: unset;">
					<img class="card-img-top" style="max-height:250px" src="img/blog1.jpg" alt="Card image cap">
					<div class="dating-card-body text-center">
						<h1 class="card-title text-white mt-3">What is Singing Hands all about?</h1>
						<p class="">It all begins with an idea. Maybe you want to launch a business. Maybe you want to turn a hobby into something more. Or maybe you have a creative project to share with the world. Whatever it is, the way you tell your story online can make all the difference.</p>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-6">
				<div class="card" style="width: 100%;background: unset;border: unset;">
					<img class="card-img-top" style="max-height:250px" src="img/blog2.jpg" alt="Card image cap">
					<div class="dating-card-body text-center">
						<h1 class="card-title text-white mt-3">More about Step By Step</h1>
						<p class="">It all begins with an idea. Maybe you want to launch a business. Maybe you want to turn a hobby into something more. Or maybe you have a creative project to share with the world. Whatever it is, the way you tell your story online can make all the difference.</p>
					</div>
				</div>
			</div>
			<div class="col-12 text-center mt-5">
				<a href="#" class="btn btn-black btn-lg">Make it</a>
			</div>
		</div>
	</div>
</section>

<!-- start footer Area -->
<?php include('includes/main_footer.php'); ?>