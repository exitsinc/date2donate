          <footer class="footer-area section-gap">
                <div class="container">
                    <div class="row align-items-center ">
                        
                        
                        <div class="col-lg-12 col-md-12 col-sm-6 social-widget">
                            <div class="single-footer-widget">
                                <h4>All Rights Reserved to Date2Donate 2020</h4>
                                <!-- <p>Let us be social</p> -->
                                <div class="footer-social d-flex align-items-center">
                                    <a href="https://www.facebook.com/Date2donate/" target="_blank" class="m-2"><i class="fa fa-facebook fa-2x"></i></a>
                                    <a href="https://www.instagram.com/datetodonate/" target="_blank" class="m-2"><i class="fa fa-instagram fa-2x"></i></a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End footer Area -->		

			<script src="js/vendor/jquery-2.2.4.min.js"></script>
			<script src="js/popper.min.js"></script>
			<script src="js/vendor/bootstrap.min.js"></script>			
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>			
  			<script src="js/easing.min.js"></script>			
			<script src="js/hoverIntent.js"></script>
			<script src="js/superfish.min.js"></script>	
			<script src="js/jquery.ajaxchimp.min.js"></script>
			<script src="js/jquery.magnific-popup.min.js"></script>	
    		<script src="js/jquery.tabs.min.js"></script>						
			<script src="js/jquery.nice-select.min.js"></script>	
            <script src="js/isotope.pkgd.min.js"></script>			
			<script src="js/waypoints.min.js"></script>
			<script src="js/jquery.counterup.min.js"></script>
			<script src="js/simple-skillbar.js"></script>							
			<script src="js/owl.carousel.min.js"></script>							
			<script src="js/mail-script.js"></script>	
			<script src="js/main.js"></script>	
		</body>
	</html>